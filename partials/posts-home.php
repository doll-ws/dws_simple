<?php
/**
 * Partial: Posts Category Home
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

$getCatId = get_theme_mod( 'home_news_cat' , 1 );
$homeId = $getCatId;
$idObj = get_category($getCatId);
$homeName = $idObj->name;
$homeCount = $idObj->count;


$getNewsTitle = get_theme_mod( 'home_news_shown_title' , __('News', 'dws_simple') );
$getNewsCountPerPage = get_theme_mod( 'home_news_count_per_page' , 5 );

$catShownName = (
    $getNewsTitle != ''
    ? $getNewsTitle
    : $homeName
);

$args = array('posts_per_page' => $getNewsCountPerPage, 'category' => $homeId);

$homePosts = get_posts( $args );

$paged = (get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
$page = (get_query_var( 'page' )) ? get_query_var( 'page' ) : 1;

if($paged === 1 && $page > $paged) {
    $paged = $page;
}

$args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'cat' => $homeId,
    'posts_per_page' => $getNewsCountPerPage,
    'paged' => $paged,
);

if($homeId === 0) {
    unset($args['cat']);
}

$wpQuery_posts = new WP_Query( $args );

//echo '<pre>' . print_r($paged, true) . '</pre>';
//echo '<pre>' . print_r($page, true) . '</pre>';
//echo '<pre>' . print_r(get_query_var( 'page' ), true) . '</pre>';
//echo '<pre>' . print_r($wpQuery_posts->posts, true) . '</pre>';
//echo '<pre>' . print_r($homePosts, true) . '</pre>';

if ( count($homePosts) ) : ?>
    <div class="content-inner col-sm-12">
        <h2><?php echo $catShownName; ?></h2>
        <div class="home-posts">

        <?php // get_template_part( 'partials/posts', 'loop' ); ?>
        <?php $setPosts = $homePosts; include( locate_template( 'partials' . DIRECTORY_SEPARATOR . 'posts-loop.php' ) ); ?>

        </div>
    </div>
<?php endif; ?>
