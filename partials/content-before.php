<?php
/**
 * Partial: Content before
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

$page = get_post();

$fbc_content = get_field( 'before_content', $page->ID );

if ( $fbc_content ) : ?>
    <section class="bc-wrapper">
        <div class="container">
            <div class="bc_inner">
                <?php echo $fbc_content; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
