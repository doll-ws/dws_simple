<?php
/**
 * Partial: Home Sites
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

$is_wpms = ( function_exists( 'is_multisite' ) && is_multisite() );

$main_site_id = get_main_site_id();

$site    = ( function_exists( 'get_site' ) && class_exists( 'WP_Site' ) ) ? get_site() : null;
$site_id = ( is_object($site) && property_exists( $site, 'blog_id' ) ) ? $site->blog_id : null;

$sites   = ( function_exists( 'dws_simple_get_sites' ) ) ? dws_simple_get_sites(false) : array();

if ( $is_wpms && $site_id == $main_site_id && count( $sites ) ) : ?>
<section class="accordian-wrapper">
    <div class="container">
        <div class="accordian sites<?php echo '-' . count($sites); ?>">
            <ul>
                <?php foreach( $sites as $subSite ) : if(is_array($subSite)) : ?>
                <?php
                    /** @var $subSite_blogDetails WP_Site */
                    $subSite_blogDetails = ( array_key_exists('blog_details',$subSite) ) ? $subSite['blog_details'] : null;

                    $subSite_id          = ( array_key_exists('ID',$subSite) ) ? $subSite['ID'] : null;

                    $SubSite_blogName    = ( is_object($subSite_blogDetails) && property_exists( $subSite_blogDetails, 'blogname' ) ) ? $subSite_blogDetails->blogname : '';
                    $SubSite_blogUrl     = ( is_object($subSite_blogDetails) && property_exists( $subSite_blogDetails, 'siteurl' ) ) ? $subSite_blogDetails->siteurl : '';
                    $SubSite_dfiUrl      = ( array_key_exists('dfi_url',$subSite)) ? $subSite['dfi_url'] : '';
                    $SubSite_msiUrl      = ( array_key_exists('msi_url',$subSite)) ? $subSite['msi_url'] : '';
                ?>
                <li data-siteid="<?php echo $subSite_id; ?>">
                    <div class="image_title">
                        <a href="<?php echo $SubSite_blogUrl; ?>" target="_self"><?php echo $SubSite_blogName; ?></a>
                    </div>
                    <a href="<?php echo $SubSite_blogUrl; ?>" target="_self">
                        <img src="<?php echo $SubSite_msiUrl; ?>"/>
                    </a>
                </li>

                <?php endif; endforeach; ?>
            </ul>
        </div>
    </div>
</section>
<?php endif; ?>
