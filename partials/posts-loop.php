<?php
/**
 * Partial: Posts Loop
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */
$curPost = get_post();
$post_id = $curPost->ID;

$prev_posts_link = '';
$next_posts_link = '';

$wpQuery_posts = (isset($wpQuery_posts) && $wpQuery_posts instanceof WP_Query) ? $wpQuery_posts : false;
$setPosts = (isset($setPosts) && is_array($setPosts)) ? $setPosts : array();

if($wpQuery_posts !== false && $wpQuery_posts->have_posts()) {

    $setPosts = array();

    while ( $wpQuery_posts->have_posts() ) :
        $wpQuery_posts->the_post();
        $post = get_post();

        $setPosts[] = $post;

    endwhile;

} else {

    if (!count($setPosts)) {
        while (have_posts()) : the_post();
            /** @var $post WP_Post */
            $post = get_post();
            $postType = (is_object($post) && property_exists($post, 'post_type')) ? $post->post_type : null;

            if (is_string($postType) && $postType == 'post') {
                $setPosts[] = $post;
            }
        endwhile;
    }
}

if (count($setPosts)) : ?>

    <?php $ids = array();
    foreach ($setPosts as $post) : setup_postdata($post); ?>
        <?php
        $ids[] = $post->ID;

        $postExcerpt = (is_object($post) && property_exists($post, 'post_excerpt')) ? trim($post->post_excerpt) : '';
        $get_theExcerpt = get_the_excerpt($post);
        $postContent = (is_object($post) && property_exists($post, 'post_content')) ? $post->post_content : '';

        $postContent = apply_filters('the_content', $postContent);
        $postContent = str_replace(']]>', ']]>', $postContent);

        $hasExcerpt = ($postExcerpt !== '');

        $hasContent = ($postContent !== '');

        if (
            !$hasExcerpt &&
            $get_theExcerpt != ''
//                && strpos($get_theExcerpt, '[&hellip;]') !== false
        ) {
            $postExcerpt = $get_theExcerpt;
            $hasExcerpt = true;
        }
        ?>
        <div class="blog-post">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <?php // the_title( '<h3 class="panel-title blog-post-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
                    <?php the_title('<h3 class="panel-title blog-post-title">', '<small class="blog-post-meta pull-right-sm">' . __('Published on', 'dws_simple') . ' ' . get_the_date('', $post) . '</small></h3>'); ?>
                </div>
                <?php if (has_post_thumbnail($post->ID)) : ?>
                <div class="row row-eq-height">
                    <div class="col-sm-4 post-thumbnail"
                         style="background-image: url('<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>');">
                    </div>
                    <div class="col-sm-8">
                        <?php endif; ?>
                        <div class="panel-body">
                            <?php if ($hasExcerpt) : ?>
                                <div class="post-excerpt">
                                    <div class="pe-inner">
                                        <?php echo $postExcerpt; ?>
                                    </div>
                                </div>
                                <div class="post-content collapse" id="post_<?php echo $post->ID; ?>">
                                    <?php echo $postContent; ?>
                                </div>
                            <?php else: ?>
                                <div class="post-content">
                                    <?php echo $postContent; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <?php if ($hasExcerpt && $hasContent) : ?>
                            <div class="panel-footer">
                                <?php /* <a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"><?php echo __( 'read more...', 'dws_simple' ); ?></a> */ ?>
                                <a href="#post_<?php echo $post->ID; ?>" data-toggle="collapse"
                                   aria-expanded="false"
                                   aria-controls="post_<?php echo $post->ID; ?>"><?php echo __('read more...', 'dws_simple'); ?></a>
                            </div>
                        <?php endif; ?>
                        <?php if (has_post_thumbnail($post->ID)) : ?>
                    </div>
                </div>
            <?php endif; ?>
            </div>
        </div><!-- /.blog-post -->
    <?php endforeach;

    if(function_exists('wp_pagenavi')) {
        try {

            if($wpQuery_posts !== false && $wpQuery_posts->have_posts()) {
                wp_pagenavi(
                    array(
                        'query' => $wpQuery_posts,
                    )
                );
            } else {
                wp_pagenavi();
            }


        } catch (Exception $e) {

            $prev_posts_link = get_next_posts_link(__('Previous', 'dws_simple'));
            $next_posts_link = get_previous_posts_link(__('Next', 'dws_simple'));

        }
    } else {

        $prev_posts_link = get_next_posts_link(__('Previous', 'dws_simple'));
        $next_posts_link = get_previous_posts_link(__('Next', 'dws_simple'));

    ?>
    <nav>
        <ul class="pager">
            <li><?php echo $prev_posts_link; ?></li>
            <li><?php echo $next_posts_link; ?></li>
        </ul>
    </nav>
    <?php
    }
endif;
