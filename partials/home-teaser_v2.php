<?php
/**
 * Partial: Home Teaser v2.0
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

$htBoxes = array();
$pages = get_pages();
foreach ( $pages as $page ) {
    $htShow   = get_field( 'visible', $page->ID );
    $pageLink = get_page_link( $page->ID );
    $htImgSrc = get_field( 'image', $page->ID );
    $htText   = get_field( 'text', $page->ID );
    $htSort   = get_field( 'sorting', $page->ID );
    $htTitle  = $page->post_title;

    if($htShow) {
        $htBoxes[$page->ID]['link']  = $pageLink;
        $htBoxes[$page->ID]['img']   = $htImgSrc;
        $htBoxes[$page->ID]['text']  = $htText;
        $htBoxes[$page->ID]['sort']  = $htSort;
        $htBoxes[$page->ID]['title'] = $htTitle;
    }
}

$getHomeTeaserLayout  = get_theme_mod( 'home_teaser_layout' , 'container' );
$htColClass = ($getHomeTeaserLayout !== 'container') ? 'col-sm-4 col-md-3 col-lg-2' : 'col-sm-4';

if(count($htBoxes)) :

    usort($htBoxes, function($a, $b) {
        return $a['sort'] - $b['sort'];
    });

    ?>
    <div class="home-teaser-wrapper">
        <div class="<?php echo $getHomeTeaserLayout; ?>">
            <div class="row">

                <?php foreach($htBoxes as $pageID => $htBox) : ?>

                    <div class="<?php echo $htColClass; ?>">
                        <figure class="home-teaser v2">
                            <img src="<?php echo $htBox['img']; ?>" alt="htimage<?php echo $pageID; ?>"/>
                            <figcaption>
                                <div class="image">
                                    <img src="<?php echo $htBox['img']; ?>" alt="htimage<?php echo $pageID; ?>"/>
                                </div>
                                <h2><?php echo $htBox['text']; ?></h2>
                            </figcaption>
                            <a href="<?php echo $htBox['link']; ?>"></a>
                        </figure>
                    </div>

                <?php endforeach; ?>

            </div>
        </div>
    </div>
<?php endif; ?>
