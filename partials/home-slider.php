<?php
/**
 * Partial: Home Slider
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

$page = get_post();

$fields = get_field_objects($page->ID);

$hsItems = array();
foreach($fields as $field) {
    $field_name = $field['name'];
    $field_value = $field['value'];

    if(strpos($field_name, 'slider_item') !== false) {
        $field_name_trimmed = str_replace('slider_item_','', $field_name);

        $field_name_parts = preg_split('/_/', $field_name_trimmed, -1, PREG_SPLIT_NO_EMPTY);

        if(count($field_name_parts)) {
            $itemId    = (int)$field_name_parts[0];
            $itemName  = (array_key_exists(1,$field_name_parts)) ? $field_name_parts[1] : '';
            $itemValue = $field_value;

            if($itemId > 0 && $itemName != '') {
                $hsItems[$itemId][$itemName] = $itemValue;
            }
        }
    }
}

$slider_type          = get_theme_mod( 'home_slider_type' ,          'slide');
$slider_speed         = get_theme_mod( 'home_slider_speed' ,         500);
$slider_autoplay      = get_theme_mod( 'home_slider_autoplay' ,      'true');
$slider_autoplaySpeed = get_theme_mod( 'home_slider_autoplayspeed' , 2000);
$slider_arrows        = get_theme_mod( 'home_slider_arrows' ,        'true');
$slider_dots          = get_theme_mod( 'home_slider_dots' ,          'true');
$slider_aHeight       = 'true';

$dataSlick  = '{';
$dataSlick .= ($slider_type == 'fade') ? '"fade":true,' : '';
$dataSlick .= '"speed":' .         $slider_speed . ',';
$dataSlick .= '"autoplay":' .      $slider_autoplay . ',';
$dataSlick .= '"autoplaySpeed":' . $slider_autoplaySpeed . ',';
$dataSlick .= '"arrows":' .        $slider_arrows . ',';
$dataSlick .= '"dots":' .          $slider_dots . ',';
$dataSlick .= '"adaptiveHeight":' . $slider_aHeight;
$dataSlick .= '}';

if(count($hsItems)) : ?>
    <section class="home-slider-wrapper">
        <div class="container-slider">
            <div class="home-slider">
                <div class="slick-slider-wrapper">
                    <div class="slick-slider slick-slider--size-full" id="slick-slider-1" data-slick='<?php echo $dataSlick; ?>'>
                <?php foreach( $hsItems as $sliderItem ) : ?>
                    <?php
                        $siLayout = (array_key_exists('layout', $sliderItem)) ? $sliderItem['layout'] : '';
                        $siImage  = (array_key_exists('image',  $sliderItem) && is_array($sliderItem['image'])) ? $sliderItem['image']  : array();
                        $siTitle  = (array_key_exists('title',  $sliderItem)) ? $sliderItem['title']  : '';
                        $siText   = (array_key_exists('text',   $sliderItem)) ? $sliderItem['text']   : '';
                        $siLink   = (array_key_exists('link',   $sliderItem)) ? $sliderItem['link']   : '';
                    ?>
                    <?php

                        $siImageId   = (array_key_exists('id',$siImage)) ? $siImage['id'] : '';
                        $siImageAlt  = (array_key_exists('alt',$siImage)) ? $siImage['alt'] : '';
                        $siImageDesc = (array_key_exists('description',$siImage)) ? $siImage['description'] : '';

                        if($siImageId != '') : ?>
                        <div class="slide">
                            <div class="slider-item">
                            <?php if($siLink != '' && $siLayout == 'only_image'): ?>
                                <a href="<?php echo $siLink; ?>">
                                    <?php echo get_image_tag( $siImageId, $siImageAlt, $siImageDesc, '', null ); ?>
                                </a>
                            <?php else: ?>
                                <?php echo get_image_tag( $siImageId, $siImageAlt, $siImageDesc, '', null ); ?>
                                <?php if($siLayout == 'left' || $siLayout == 'right'): ?>
                                    <div class="si-caption-wrapper">
                                        <div class="si-caption <?php echo $siLayout; ?> ">
                                            <div class="sic-inner center-vertical">
                                                <h3 class="sic-title">
                                                    <?php echo $siTitle; ?>
                                                </h3>
                                                <div class="sic-text">
                                                    <?php echo $siText; ?>
                                                </div>
                                                <?php if($siLink != ''): ?>
                                                    <div class="sic-link">
                                                        <a href="<?php echo $siLink; ?>" class="btn btn-primary">
                                                            <?php echo __( 'more...', 'dws_simple' ); ?>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
                </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>