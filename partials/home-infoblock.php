<?php
/**
 * Partial: Home Info Block
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

$infoBoxes = array();
$pages = get_pages();
foreach ( $pages as $page ) {

    $pageTitle  = $page->post_title;;

    $teamName_1 = get_field( "mannschaft_1", $page->ID );
    $teamName_2 = get_field( "mannschaft_2", $page->ID );

    $gameTitle  = get_field( "begegnung", $page->ID );
    $gameResult = get_field( "ergebnis", $page->ID );

    $subText    = get_field( "subtext", $page->ID );

    if($teamName_1) {
        $infoBoxes[$page->ID]['pageTitle']  = $pageTitle;
        $infoBoxes[$page->ID]['teamName_1'] = $teamName_1;
        $infoBoxes[$page->ID]['teamName_2'] = $teamName_2;
        $infoBoxes[$page->ID]['gameTitle']  = $gameTitle;
        $infoBoxes[$page->ID]['gameResult'] = $gameResult;
        $infoBoxes[$page->ID]['subText']    = $subText;
    }
}
if(count($infoBoxes)) : ?>
    <div class="info-block-wrapper">
        <div class="container">
            <div class="row info-blocks">

                <?php foreach($infoBoxes as $pageID => $infoBox) : ?>


                    <div class="col-sm-6">

                        <div class="panel panel-default head-block">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">
                                    <?php echo $infoBox['pageTitle']; ?><br />
                                    <small><?php echo $infoBox['gameTitle']; ?></small>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div class="headblock-subtitle">
                                    <div class="row text-center">
                                        <div class="col-xs-4 headblock-team"><?php echo $infoBox['teamName_1']; ?></div>
                                        <div class="col-xs-4 headblock-ergebnis"><?php echo $infoBox['gameResult']; ?></div>
                                        <div class="col-xs-4 headblock-team"><?php echo $infoBox['teamName_2']; ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="headblock-subtext text-center">
                                    <p><?php echo $infoBox['subText']; ?></p>
                                </div>
                            </div>
                        </div>

                    </div>

                <?php endforeach; ?>

            </div>
        </div>
    </div>
<?php endif; ?>