<?php
/**
 * Theme Part: Index
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */
?>
<?php get_header(); ?>
<?php

$catName = 'Blog';
$isJobs = false;
if (is_category()) {
    $category = get_category(get_query_var('cat'));
    $cat_id = $category->cat_ID;
    $catName = $category->name;

    $isJobs = ($catName == 'Jobs');
}

$isActiveSideBar = is_active_sidebar('sidebar');

$contentInnerColClass = ($isActiveSideBar) ? 'col-sm-8' : 'col-sm-12';

?>

<section class="content-wrapper cleafix">
    <div id="content" class="<?php echo get_theme_mod( 'page_layout' , 'container' ); ?>">
        <div class="row">
            <div class="content-inner <?php echo $contentInnerColClass; ?>">

                <h1 class="page-header"><?php echo $catName; ?></h1>

                <?php if($isJobs) : ?>
                    <?php get_template_part( 'partials/posts', 'jobs' ); ?>
                <?php else: ?>
                    <?php get_template_part( 'partials/posts', 'default' ); ?>
                <?php endif; ?>

            </div>

            <?php if($isActiveSideBar) : ?>
            <div class="sidebar col-sm-4">
                <?php get_sidebar(); ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>