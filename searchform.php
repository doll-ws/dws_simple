<?php
/**
 * Theme Part: Search Form
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */
?>
<form role="search" method="get" class="searchform form-inline" action="<?php echo home_url( '/' ); ?>">

    <div class="form-group">
        <label class="sr-only" for="search_field"><?php echo __( 'Search for:', 'dws_simple' ) ?></label>
        <div class="input-group">
            <input type="search" class="form-control search-field"
                 placeholder="<?php echo __( 'Search', 'dws_simple' ) ?>"
                 value="<?php echo get_search_query() ?>" name="s"
                 title="<?php echo __( 'Search for:', 'dws_simple' ) ?>" />
            <div class="input-group-btn">
                <button type="submit" class="btn btn-primary">
                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </button>
            </div>
        </div>
    </div>

</form>