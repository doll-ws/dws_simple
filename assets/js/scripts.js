/**
 * Theme JavaScript
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function eleDistanceTop(selector) {
    selector = typeof selector !== 'undefined' ? selector : '';

    var distTop = 0;
    if(selector !== '' && $(selector).length) {
        var bordT = jQuery(selector).css('border-top-width').replace('px','');
        var paddT = jQuery(selector).css('padding-top').replace('px','');
        var margT = jQuery(selector).css('margin-top').replace('px','');

        distTop = parseInt(bordT) + parseInt(paddT) + parseInt(margT);
    }

    return distTop;
}

function stickyHeader(selector,distanceTop){
    selector = typeof selector !== 'undefined' ? selector : 'body';
    distanceTop = typeof distanceTop !== 'undefined' ? distanceTop : 0;

    if(selector !== '' && jQuery(selector).length) {
        var scrollTop = jQuery(window).scrollTop();
        var stickyNavHeight = jQuery('.header').outerHeight(true);

        if (scrollTop > distanceTop) {
            jQuery(selector).addClass('stickyHeader');
            jQuery('body').css({ paddingTop: stickyNavHeight + 'px'});
        } else {
            jQuery(selector).removeClass('stickyHeader');
            jQuery('body').removeAttr('style');
        }
    }
}

jQuery( document ).ready(function($) {

    if($('#back-to-top').length) {
        $('#back-to-top').click( function (ele) {
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            event.preventDefault();
            return false;
        });
    }

    /** Sticky Header */
    var stickyAnchorTop = $('#top').offset().top + 50;

    stickyHeader('body',stickyAnchorTop);

    $(window).scroll(function() {
        stickyHeader('body',stickyAnchorTop);
    });

    if($('.blog-post .panel .post-excerpt').length) {
        var post_excerpts = $('.blog-post .panel .post-excerpt');
        $.each(post_excerpts, function(i, pe_ele){
            var post_excerpt = $(pe_ele);
            var pe_inner = post_excerpt.find('.pe-inner');
            var the_excerpt = (pe_inner.length) ? pe_inner : post_excerpt;
            var post_panel_body = post_excerpt.parent();
            var panel = post_panel_body.parent();

            if(!panel.hasClass('panel')) {
                var row = panel.parent();
                var panel = row.parent();
            }

            var post_content = post_panel_body.find('.post-content');

            if(
                post_content.length &&
                post_content.hasClass('collapse')
            ) {
                post_content.on('show.bs.collapse', function () {
                    if(the_excerpt.text().indexOf('[…]') !== -1) {
                        if(pe_inner.length) {
                            pe_inner.fadeOut();
                        }
                        post_excerpt.slideUp('fast');
                    }
                    panel.addClass('expanded');
                });
                post_content.on('hide.bs.collapse', function () {
                    if(the_excerpt.text().indexOf('[…]') !== -1) {
                        if(pe_inner.length) {
                            pe_inner.fadeIn();
                        }
                        post_excerpt.slideDown('fast');
                    }
                    panel.removeClass('expanded');
                });
            }
        });
    }

});