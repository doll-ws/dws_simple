<?php
/**
 * CSS Styles: Styles via PHP SCSS Compiler
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 2.4
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Set Header to CSS */
header("Content-type: text/css; charset: UTF-8");

/** Include WordPress Load */
define( 'WP_USE_THEMES', false );

/** Resolve $wpCoreDir */
$ds = DIRECTORY_SEPARATOR;
$thisDir = rtrim(__DIR__, $ds);

$themeDirName = (strpos($thisDir, 'simple-theme') !== false) ? 'simple-theme' : 'simple_theme';
$stylesCssPhpRelPath = $themeDirName . $ds . 'assets' . $ds . 'css';

$wpContentRelPath = 'wp-content';
$wpRepPath = '';

if (strpos($thisDir, 'web' . $ds . 'app' . $ds . 'themes') !== false) {
	$wpContentRelPath = 'app';
	$wpRepPath = 'wp';
}

$search = $wpContentRelPath . $ds . 'themes' . $ds . $stylesCssPhpRelPath;
$replace = $wpRepPath;
$wpCoreDir = rtrim(str_replace($search,$replace, $thisDir), $ds);

include(rtrim($wpCoreDir, $ds) . $ds . 'wp-load.php');

/** Get HTTP Query */
$get = (is_array($_GET)) ? $_GET : array();

/** Set Variables Array for SCSS Compiler */
$scssVars = array();
$defaultPageColors = dws_simple_get_default_page_colors();
foreach($defaultPageColors as $dpcKey => $dpc) {
    $key      = $dpc['key'];
    $value    = $dpc['color'];
    $variable = $dpc['var'];

    $scssVars[$variable] = (array_key_exists($key, $get)) ? $get[$key] : get_theme_mod( $key , $value );
}

/**
 * SCSS Compiler
 */

/** Get SCSS Compiler */
require'../../inc/scssphp/scss.inc.php';

/** @var $scss scssc */
$scss = new scssc();

/** Minify Output */
$scss->setFormatter('scss_formatter_compressed');

/** Scss Import Path */
$scss->setImportPaths('../scss/');

/** Set Custom Color Variables */
$scss->setVariables($scssVars);

$compile = '@import "base.scss";';

$mobileMenuBreakpoint = (int) get_theme_mod('mobile_breakpoint', '768');
if($mobileMenuBreakpoint > 768) {

    $maxWidth = $mobileMenuBreakpoint -1;
    $importResponsiveToggleNavStyles = '@import "responsive' . DIRECTORY_SEPARATOR . 'toggle-nav-until_' . $maxWidth . '.scss";';

    $compile .= PHP_EOL . $importResponsiveToggleNavStyles;
}

//wp_die($compile);

/** Output Styles */
echo $scss->compile($compile);
