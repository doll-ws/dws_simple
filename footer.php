<?php
/**
 * Theme Part: Footer
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 2.4
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */
?>
<?php if ( is_active_sidebar( 'footer-top' ) ) : ?>
<div class="footer-top clearfix">
    <div class="footer-top-inner">
        <div class="<?php echo get_theme_mod( 'page_layout' , 'container' ); ?>">
            <?php dynamic_sidebar( 'footer-top' ); ?>
        </div>
    </div>
</div>
<?php endif; ?>
<footer class="footer">
    <div class="footer-inner">
        <div class="<?php echo get_theme_mod( 'page_layout' , 'container' ); ?>">
            <div class="row">
                <div class="col-sm-6 fi-left clearfix">
                    <?php if ( is_active_sidebar( 'footer-left' ) ) { dynamic_sidebar( 'footer-left' ); } ?>
                </div>
                <div class="col-sm-6 fi-right">
                    <?php if ( is_active_sidebar( 'footer-right' ) ) { dynamic_sidebar( 'footer-right' ); } ?>
                </div>
            </div>
        </div>
    </div>
</footer>
</div> <!-- end Wrapper -->
<?php wp_footer(); ?>
</body>
</html>
