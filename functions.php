<?php
/**
 * Main Functions File
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Theme Setup */
require_once('functions/functions-setup.php');

/** MultiSite Functions */
require_once('functions/functions-multisite.php');

/** Register Menu */
require_once('functions/functions-menu.php');

/** Theme Customizer */
require_once('functions/functions-customizer.php');

/** Styles & Scripts */
require_once('functions/functions-scripts.php');

/** Widgets Init */
require_once('functions/functions-widgets.php');

/** Plugin Actions */
require_once('functions/functions-pluginActions.php');

/** Additional Functions */
require_once('functions/functions-additional.php');

/** Debug */
//require_once('functions/functions-debug.php');
