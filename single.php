<?php
/**
 * Theme Part: Single
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */
?>
<?php get_header(); ?>
<?php
$post_id = get_the_id();

$category = dws_simple_getPostPrimaryCategory( $post_id );
$category_link = get_category_link($category);
$category_name = $category->name;
?>
<div class="blog">
    <section class="content-wrapper cleafix">
        <div id="content" class="<?php echo get_theme_mod( 'page_layout' , 'container' ); ?>">
            <div class="row">
                <div class="content-inner col-sm-8">
                    <h1 class="page-header blog-post-title">
                        <?php the_title(); ?>
                    </h1>

                    <div class="blog-main">

                        <?php
                            if ( have_posts() ) {
                                while ( have_posts() ) : the_post();
                        ?>
                            <div class="blog-post">
                                <p class="blog-post-meta"><?php the_date(); ?></p>
                                <?php if(has_excerpt()): ?>
                                    <div class="panel panel-success">
                                        <div class="panel-body">
                                            <?php the_excerpt(); ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php the_content(); ?>
                            </div><!-- /.blog-post -->
                            <?php
                                endwhile;
                            }
                        ?>

                        <?php if($category_link) : ?>
                            <hr />
                            <div class="btn-set text-right">
                                <a href="<?php echo $category_link; ?>" class="btn btn-primary btn-sm">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                    <?php echo __( 'Back to overview', 'dws_simple' ); ?>
                                </a>
                            </div>
                        <?php endif; ?>

                    </div><!-- /.blog-main -->

                </div>

                <div class="sidebar col-sm-4">
                    <?php get_sidebar(); ?>
                </div>
            </div>
    </section>
</div>
<?php get_footer(); ?>