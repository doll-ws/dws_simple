#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Doll WebSolutions - Simple Theme\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-04-26 13:10+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#: single.php:69
msgid "Back to overview"
msgstr ""

#: partials/posts-jobs.php:48 partials/posts-jobs.php:60
msgid "Close"
msgstr ""

#. Name of the template
msgid "Contact"
msgstr ""

#: functions/functions-customizer.php:34
msgid "Default Layout"
msgstr ""

#: functions/functions-customizer.php:59
msgid "do not show"
msgstr ""

#. Author of the theme
msgid "Doll WebSolutions"
msgstr ""

#. Name of the theme
msgid "Doll WebSolutions - Simple Theme"
msgstr ""

#: functions/functions-widgets.php:47
msgid "Footer - Left"
msgstr ""

#: functions/functions-widgets.php:56
msgid "Footer - right"
msgstr ""

#: functions/functions-menu.php:30
msgid "Header Menu"
msgstr ""

#: functions/functions-widgets.php:29
msgid "Home - Sidebar"
msgstr ""

#: functions/functions-customizer.php:49
msgid "Home-Sidebar"
msgstr ""

#. Name of the template
msgid "Homepage"
msgstr ""

#. Author URI of the theme
msgid "https://www.doll-ws.com/"
msgstr ""

#: partials/posts-jobs.php:39
msgid "more..."
msgstr ""

#: header.php:64
msgid "Navigation"
msgstr ""

#: functions/functions-customizer.php:38
msgid "Page Layout"
msgstr ""

#: functions/functions-customizer.php:30
msgid "Page Settings"
msgstr ""

#: partials/posts-jobs.php:35
msgid "Posted on"
msgstr ""

#: partials/posts-default.php:35
msgid "Published on"
msgstr ""

#: partials/posts-default.php:40
msgid "read more..."
msgstr ""

#: searchform.php:31
msgid "Search"
msgstr ""

#: searchform.php:28 searchform.php:33
msgid "Search for:"
msgstr ""

#: functions/functions-customizer.php:58
msgid "show"
msgstr ""

#: functions/functions-customizer.php:53
msgid "Show Home-Sidebar"
msgstr ""

#: functions/functions-widgets.php:38
msgid "Sidebar - Default"
msgstr ""
