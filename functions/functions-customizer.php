<?php
/**
 * functions-customizer.php
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Default Page Colors */
function dws_simple_get_default_page_colors() {
    /** Basic Colors - Hex Values */
    $primaryColor               = '#add255';
    $secondaryColor             = '#00add9';
    $tertiaryColor              = '#f60929';
    $bodyBackgroundColor        = '#ffffff';
    $mainContentBackgroundColor = '#ffffff';
    $mainContentTextColor       = '#000000';

    return array(

        /** Page Appearance - Primary Color */
        'page_primary_color' => array(
            'key'   => 'page_primary_color',
            'var'   => 'primaryColor',
            'name'  => __( 'Primary Color', 'dws_simple' ),
            'color' => $primaryColor,
        ),

        /** Page Appearance - Secondary Color */
        'page_secondary_color' => array(
            'key'   => 'page_secondary_color',
            'var'   => 'secondaryColor',
            'name'  => __( 'Secondary Color', 'dws_simple' ),
            'color' => $secondaryColor,
        ),

        /** Page Appearance - Body Background Color */
        'page_bodybg_color' => array(
            'key'   => 'page_bodybg_color',
            'var'   => 'bodyBackgroundColor',
            'name'  => __( 'Body Background Color', 'dws_simple' ),
            'color' => $bodyBackgroundColor,
        ),

        /** Page Appearance - Header Background Color */
        'page_headerbg_color' => array(
            'key'   => 'page_headerbg_color',
            'var'   => 'headerBackgroundColor',
            'name'  => __( 'Header Background Color', 'dws_simple' ),
            'color' => $primaryColor,
        ),

        /** Page Appearance - Navigation Background Color */
        'page_navbg_color' => array(
            'key'   => 'page_navbg_color',
            'var'   => 'navBackgroundColor',
            'name'  => __( 'Navigation Background Color', 'dws_simple' ),
            'color' => $primaryColor,
        ),

        /** Page Appearance - Navigation Background Color Active */
        'page_navbg_color_active' => array(
            'key'   => 'page_navbg_color_active',
            'var'   => 'navBackgroundColorActive',
            'name'  => __( 'Navigation Background Color Active', 'dws_simple' ),
            'color' => $primaryColor,
        ),

        /** Page Appearance - Navigation Text Color */
        'page_navtxt_color' => array(
            'key'   => 'page_navtxt_color',
            'var'   => 'navTextColor',
            'name'  => __( 'Navigation Text Color', 'dws_simple' ),
            'color' => $secondaryColor,
        ),

        /** Page Appearance - Navigation Text Color Active */
        'page_navtxt_color_active' => array(
            'key'   => 'page_navtxt_color_active',
            'var'   => 'navTextColorActive',
            'name'  => __( 'Navigation Text Color Active', 'dws_simple' ),
            'color' => $tertiaryColor,
        ),

        /** Page Appearance - Main Content Background Color */
        'page_maincontentbg_color' => array(
            'key'   => 'page_maincontentbg_color',
            'var'   => 'mainContentBackgroundColor',
            'name'  => __( 'Main Content Background Color', 'dws_simple' ),
            'color' => $mainContentBackgroundColor,
        ),

        /** Page Appearance - Main Content Text Color */
        'page_maincontenttxt_color' => array(
            'key'   => 'page_maincontenttxt_color',
            'var'   => 'mainContentTextColor',
            'name'  => __( 'Main Content Text Color', 'dws_simple' ),
            'color' => $mainContentTextColor,
        ),

        /** Page Appearance - Sidebar Background Color */
        'page_sidebarbg_color' => array(
            'key'   => 'page_sidebarbg_color',
            'var'   => 'sidebarBackgroundColor',
            'name'  => __( 'Sidebar Background Color', 'dws_simple' ),
            'color' => $primaryColor,
        ),

        /** Page Appearance - Sidebar Text Color */
        'page_sidebartxt_color' => array(
            'key'   => 'page_sidebartxt_color',
            'var'   => 'sidebarTextColor',
            'name'  => __( 'Sidebar Text Color', 'dws_simple' ),
            'color' => $secondaryColor,
        ),

        /** Page Appearance - Footer Background Color */
        'page_footerbg_color' => array(
            'key'   => 'page_footerbg_color',
            'var'   => 'footerBackgroundColor',
            'name'  => __( 'Footer Background Color', 'dws_simple' ),
            'color' => $primaryColor,
        ),

        /** Page Appearance - Footer Text Color */
        'page_footertxt_color' => array(
            'key'   => 'page_footertxt_color',
            'var'   => 'footerTextColor',
            'name'  => __( 'Footer Text Color', 'dws_simple' ),
            'color' => $secondaryColor,
        ),
    );
}

/** Theme Customizer */
function dws_simple_page_settings($wp_customize){

    /** Simple Theme - Settings Panel */
    $wp_customize->add_panel( 'page_settings_panel', array(
        'title'       => __( 'Simple Theme - Settings', 'dws_simple' ),
        'description' => __( 'Here you can set the Look and Feel for your Website.', 'dws_simple' ),
        'priority' => 130
    ) );


    /**
     * Page Layout Section
     */
    $wp_customize->add_section( 'page_layout_section', array(
        'title' => __( 'Page Layout', 'dws_simple' ),
        'panel'   => 'page_settings_panel',
    ));

    /** Page Layout - Responsive Type */
    $wp_customize->add_setting( 'page_layout', array(
        'default' => 'container',
    ));

    $wp_customize->add_control( 'page_layout_option', array(
        'label'    => __( 'Page Layout', 'dws_simple' ),
        'section'  => 'page_layout_section',
        'settings' => 'page_layout',
        'type'     => 'select',
        'choices'  => array(
            'container' => 'Responsive',
            'container-fluid' => 'Responsive Fluid',
        ),
    ) );

    /** Page Header Layout - Responsive Type */
    $wp_customize->add_setting( 'header_layout', array(
        'default' => 'container',
    ));

    $wp_customize->add_control( 'header_layout_option', array(
        'label'    => __( 'Header Layout', 'dws_simple' ),
        'section'  => 'page_layout_section',
        'settings' => 'header_layout',
        'type'     => 'select',
        'choices'  => array(
            'container' => 'Responsive',
            'container-fluid' => 'Responsive Fluid',
        ),
    ) );

    /** Page Header Layout - Responsive Type */
    $wp_customize->add_setting( 'mobile_breakpoint', array(
        'default' => '768',
    ));

    $wp_customize->add_control( 'mobile_breakpoint_option', array(
        'label'       => __( 'Mobile Breakpoint', 'dws_simple' ),
        'description' => __( 'Show "Menu Hamburger" on specific screen size.', 'dws_simple' ),
        'section'     => 'page_layout_section',
        'settings'    => 'mobile_breakpoint',
        'type'        => 'select',
        'choices'     => array(
            '768' => '768px (Mobile)',
            '992' => '992px (Tablet)',
        ),
    ) );


    /**
     * Page Appearance Section
     */
    $wp_customize->add_section( 'page_appearance_section', array(
        'title' => __( 'Page Appearance', 'dws_simple' ),
        'panel'   => 'page_settings_panel',
    ));

    /** Page Appearance - Logo */
    $wp_customize->add_setting( 'page_logo', array(
        'default'   => '',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'page_logo',
        array(
            'label' => __( 'Logo', 'dws_simple' ),
            'description' => esc_html__( 'Upload the Logo File for your Page', 'dws_simple' ),
            'section' => 'page_appearance_section',
        )
    ) );

    /** Page Appearance - BG Image */
    $wp_customize->add_setting( 'page_bg_image', array(
        'default'   => '',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'page_bg_image',
        array(
            'label'       => __( 'Background Image', 'dws_simple' ),
            'description' => esc_html__( 'Upload the Background Image File for your Page', 'dws_simple' ),
            'section'     => 'page_appearance_section',
        )
    ) );

    /** Page Appearance - Theme Colors */
    $defaultPageColors = dws_simple_get_default_page_colors();
    foreach($defaultPageColors as $dpcKey => $dpc) {
        $wp_customize->add_setting($dpc['key'], array(
            'default'           => $dpc['color'],
            'transport'         => 'refresh',
            'sanitize_callback' => 'sanitize_hex_color',
        ));

        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, $dpc['key'],
            array(
                'label'   => $dpc['name'],
                'section' => 'page_appearance_section',
            )
        ));
    }

    /**
     * Home Section
     */
    $wp_customize->add_section( 'home_section', array(
        'title' => __( 'Homepage', 'dws_simple' ),
        'panel'   => 'page_settings_panel',
    ));

    /** Home - Show Sidebar */
    $wp_customize->add_setting( 'home_sidebar', array(
        'default' => 1,
    ));

    $wp_customize->add_control( 'home_sidebar_option', array(
        'label' => __( 'Sidebar', 'dws_simple' ),
        'section' => 'home_section',
        'settings' => 'home_sidebar',
        'type' => 'select',
        'choices' => array(
            1 => __( 'show', 'dws_simple' ),
            0 => __( 'do not show', 'dws_simple' ),
        ),
    ) );

    /** Home - Show Teaser */
    $wp_customize->add_setting( 'home_teaser', array(
        'default' => 1,
    ));

    $wp_customize->add_control( 'home_teaser_option', array(
        'label' => __( 'Teaser Boxes', 'dws_simple' ),
        'section' => 'home_section',
        'settings' => 'home_teaser',
        'type' => 'select',
        'choices' => array(
            1 => __( 'show', 'dws_simple' ),
            0 => __( 'do not show', 'dws_simple' ),
        ),
    ) );

    /** Home - Show Teaser */
    $wp_customize->add_setting( 'home_teaser_version', array(
        'default' => 1,
    ));

    $wp_customize->add_control( 'home_teaser_version_option', array(
        'label'       => __( 'Teaser Boxes Style', 'dws_simple' ),
        'description' => __( 'Originals on LittleSnippets.net', 'dws_simple' ),
        'section'     => 'home_section',
        'settings'    => 'home_teaser_version',
        'type'        => 'select',
        'choices'     => array(
            1 => __( 'v1.0', 'dws_simple' ),
            2 => __( 'v2.0', 'dws_simple' ),
            3 => __( 'v3.0', 'dws_simple' ),
        ),
    ) );

    /** Home - Teaser Layout */
    $wp_customize->add_setting( 'home_teaser_layout', array(
        'default' => 'container',
    ));

    $wp_customize->add_control( 'home_teaser_layout_option', array(
        'label'    => __( 'Teaser Boxes  Layout', 'dws_simple' ),
        'section'  => 'home_section',
        'settings' => 'home_teaser_layout',
        'type'     => 'select',
        'choices'  => array(
            'container' => 'Responsive',
            'container-fluid' => 'Responsive Fluid',
        ),
    ) );

    /** Home - Show Home News */
    $wp_customize->add_setting( 'home_news', array(
        'default' => 1,
    ));

    $wp_customize->add_control( 'home_news_option', array(
        'label' => __( 'News', 'dws_simple' ),
        'section' => 'home_section',
        'settings' => 'home_news',
        'type' => 'select',
        'choices' => array(
            1 => __( 'show', 'dws_simple' ),
            0 => __( 'do not show', 'dws_simple' ),
        ),
    ) );

    /** Home - Set Home News Category */
    $wp_customize->add_setting( 'home_news_cat', array(
        'default' => 1,
    ));

    $catArr = dws_getCategoriesArrayForCustomizer();
    $catArr[0] = __( 'All', 'dws_simple' );
    ksort($catArr);
    $wp_customize->add_control( 'home_news_cat_option', array(
        'label' => __( 'News Category', 'dws_simple' ),
        'section' => 'home_section',
        'settings' => 'home_news_cat',
        'type' => 'select',
        'choices' => $catArr,
    ) );

    /** Home - Set Home News Shown Title */
    $wp_customize->add_setting( 'home_news_shown_title', array(
        'default' => __( 'News', 'dws_simple' ),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control( 'home_news_shown_title_option', array(
        'label' => __( 'News Shown Title', 'dws_simple' ),
        'section' => 'home_section',
        'settings' => 'home_news_shown_title',
        'type' => 'text',
    ) );

    /** Home - Set Home News Count Per Page */
    $wp_customize->add_setting( 'home_news_count_per_page', array(
        'default' => 5,
    ));

    $wp_customize->add_control( 'home_news_count_per_page_option', array(
        'label' => __( 'News Count Per Page', 'dws_simple' ),
        'section' => 'home_section',
        'settings' => 'home_news_count_per_page',
        'type' => 'select',
        'choices' => array(
            5 => 5,
            10 => 10,
            15 => 15,
            20 => 20,
        ),
    ) );

    /**
     * Home Slider Section
     */
    $wp_customize->add_section( 'home_slider_section', array(
        'title' => __( 'Homepage Slider', 'dws_simple' ),
        'panel'   => 'page_settings_panel',
    ));

    /** Home - Slider Type */
    $wp_customize->add_setting( 'home_slider_type', array(
        'default' => 'slide',
    ));

    $wp_customize->add_control( 'home_slider_type_option', array(
        'label'    => __( 'Slider - Type', 'dws_simple' ),
        'section'  => 'home_slider_section',
        'settings' => 'home_slider_type',
        'type'     => 'select',
        'choices'  => array(
            'slide' => __( 'Slide', 'dws_simple' ),
            'fade'  => __( 'Fade', 'dws_simple' ),
        ),
    ) );

    /** Home - Slider Speed */
    $wp_customize->add_setting( 'home_slider_speed', array(
        'default' => 500,
    ));

    $wp_customize->add_control( 'home_slider_speed_option', array(
        'label'    => __( 'Slider - Speed', 'dws_simple' ),
        'section'  => 'home_slider_section',
        'settings' => 'home_slider_speed',
        'type'     => 'number',
    ) );

    /** Home - Slider Autoplay */
    $wp_customize->add_setting( 'home_slider_autoplay', array(
        'default' => 'true',
    ));

    $wp_customize->add_control( 'home_slider_autoplay_option', array(
        'label'    => __( 'Slider - Autoplay', 'dws_simple' ),
        'section'  => 'home_slider_section',
        'settings' => 'home_slider_autoplay',
        'type'     => 'select',
        'choices'  => array(
            'true'  => __( 'Yes', 'dws_simple' ),
            'false' => __( 'No', 'dws_simple' ),
        ),
    ) );

    /** Home - Slider Speed */
    $wp_customize->add_setting( 'home_slider_autoplayspeed', array(
        'default' => 2000,
    ));

    $wp_customize->add_control( 'home_slider_autoplayspeed_option', array(
        'label'    => __( 'Slider - Autoplay Speed', 'dws_simple' ),
        'section'  => 'home_slider_section',
        'settings' => 'home_slider_autoplayspeed',
        'type'     => 'number',
    ) );

    /** Home - Slider Arrows */
    $wp_customize->add_setting( 'home_slider_arrows', array(
        'default' => 'true',
    ));

    $wp_customize->add_control( 'home_slider_arrows_option', array(
        'label'    => __( 'Slider - Arrows', 'dws_simple' ),
        'section'  => 'home_slider_section',
        'settings' => 'home_slider_arrows',
        'type'     => 'select',
        'choices'  => array(
            'true'  => __( 'Yes', 'dws_simple' ),
            'false' => __( 'No', 'dws_simple' ),
        ),
    ) );

    /** Home - Slider Dots */
    $wp_customize->add_setting( 'home_slider_dots', array(
        'default' => 'true',
    ));

    $wp_customize->add_control( 'home_slider_dots_option', array(
        'label'    => __( 'Slider - Dots', 'dws_simple' ),
        'section'  => 'home_slider_section',
        'settings' => 'home_slider_dots',
        'type'     => 'select',
        'choices'  => array(
            'true'  => __( 'Yes', 'dws_simple' ),
            'false' => __( 'No', 'dws_simple' ),
        ),
    ) );
}
add_action('customize_register', 'dws_simple_page_settings');

function dws_simple_get_customized() {
    $customized = array();
    $get = (is_array($_GET)) ? $_GET : array();
    $post = (is_array($_POST)) ? $_POST : array();

    if(count($get) || count($post)) {

        if(count($get)) {

            $customize_changeset_uuid    = (array_key_exists('customize_changeset_uuid', $get)) ? $get['customize_changeset_uuid'] : '';
            $customize_theme             = (array_key_exists('customize_theme', $get)) ? $get['customize_theme'] : '';
            $customize_messenger_channel = (array_key_exists('customize_messenger_channel', $get)) ? $get['customize_messenger_channel'] : '';

            $args = array(
                'changeset_uuid' => $customize_changeset_uuid,
                'theme' => $customize_theme,
                'messenger_channel' => $customize_messenger_channel,
            );

            if(class_exists('WP_Customize_Manager')) {

                /** @var $wpCustomizeManager WP_Customize_Manager */
                $wpCustomizeManager = new WP_Customize_Manager($args);

                $changeSetDataDetailed = $wpCustomizeManager->changeset_data();

                $changeSetData = array();

                foreach ($changeSetDataDetailed as $fullKey => $data) {
                    $key = str_replace($customize_theme . '::', '', $fullKey);
                    $value = (array_key_exists('value', $data)) ? $data['value'] : '';
                    $type = (array_key_exists('type', $data)) ? $data['type'] : '';

                    if ($type == 'theme_mod' && $value !== '') {
                        $changeSetData[$key] = $value;
                    }
                }

                $customized = $changeSetData;
            }
        }

        if(count($post)) {
            $post_customized = (array_key_exists('customized', $post)) ? json_decode(str_replace('\\"' , '"' , $post['customized'])) : array();

            if (count($post_customized)) {
                foreach($post_customized as $key => $value) {
                    $customized[$key] = $value;
                }
            }
        }
    }


    return $customized;
}

function dws_getCategoriesArrayForCustomizer() {
    $categoriesArrayForCustomizer = array();

    $wpCategories = get_categories();

    usort($wpCategories, function ($a, $b) {
        return $a->cat_ID - $b->cat_ID;
    });

    foreach($wpCategories as $wpCategory) {
        $categoriesArrayForCustomizer[$wpCategory->cat_ID] = $wpCategory->name . ' (' . $wpCategory->count . ') ';
    }

    return $categoriesArrayForCustomizer;
}
