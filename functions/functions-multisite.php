<?php
/**
 * functions-multisite.php
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function dws_simple_get_sites($full = true) {
    $sites = array();

    $is_wpms = ( function_exists( 'is_multisite' ) && is_multisite() );
    if(!$is_wpms) {
        return $sites;
    }

    $main_site_id = get_main_site_id();
    $curr_site_id = get_current_blog_id();

    /** Set Main Site */
    $main_site = ( function_exists( 'get_site' ) && class_exists( 'WP_Site' ) ) ? get_site( $main_site_id ) : null;
    $main_site_id = ( is_object($main_site) && property_exists( $main_site, 'blog_id' ) ) ? $main_site->blog_id : null;
    $main_blog_details = get_blog_details($main_site_id);

    switch_to_blog($main_site_id);

    $front_id = get_option('page_on_front');

    $main_multiSiteImageField = get_field( 'multi-site-image', $front_id );

    $main_multiSiteImageUrl = (
        ($main_multiSiteImageField)
        ? $main_multiSiteImageField
        : ''
    );

    $sites[$main_site_id] = array(
        'ID' => $main_site_id,
        'blog_details' => $main_blog_details,
        'dfi' => get_the_post_thumbnail(),
        'dfi_url' => get_the_post_thumbnail_url(),
        'msi_url' => $main_multiSiteImageUrl,
        'is_main' => true
    );
    switch_to_blog($curr_site_id);

    $main_site_array = $sites[$main_site_id];

    /** Set Current Site */
    $curr_site = ( function_exists( 'get_site' ) && class_exists( 'WP_Site' ) ) ? get_site( $curr_site_id ) : null;
    $curr_site_id = ( is_object($curr_site) && property_exists( $curr_site, 'blog_id' ) ) ? $curr_site->blog_id : null;
    $curr_blog_details = get_blog_details($curr_site_id);

    $front_id = get_option('page_on_front');

    $curr_multiSiteImageField = get_field( 'multi-site-image', $front_id );

    $curr_multiSiteImageUrl = (
        ($curr_multiSiteImageField)
        ? $curr_multiSiteImageField
        : $main_multiSiteImageUrl
    );

    $sites[$curr_site_id] = array(
        'ID' => $curr_site_id,
        'blog_details' => $curr_blog_details,
        'dfi' => get_the_post_thumbnail(),
        'dfi_url' => get_the_post_thumbnail_url(),
        'msi_url' => $curr_multiSiteImageUrl,
        'is_main' => ($main_site_id == $curr_site_id)
    );

    $curr_site_array = $sites[$curr_site_id];

    $get_sites = ( function_exists( 'get_sites' ) && class_exists( 'WP_Site_Query' ) ) ? get_sites( array( 'site__not_in' => array( $main_site_id , $curr_site_id ) ) ) : array();

    if ( count( $get_sites ) ) {
        foreach( $get_sites as $site ) {
            $site_id = get_object_vars($site)["blog_id"];
            $blog_details = get_blog_details($site_id);

            $isPublic = ( is_object($blog_details) && property_exists( $blog_details, 'public' ) ) ? $blog_details->public : false;

            if($isPublic) {
                switch_to_blog($site_id);

                $front_id = get_option('page_on_front');

                $multiSiteImageField = get_field( 'multi-site-image', $front_id );

                /** Fix if $multiSiteImageField returns Id */
                if(is_numeric($multiSiteImageField)) {
                    $multiSiteImageField = wp_get_attachment_url( $multiSiteImageField );
                }

                $multiSiteImageUrl = (
                    ($multiSiteImageField)
                    ? $multiSiteImageField
                    : $main_multiSiteImageUrl
                );

                $sites[$site_id] = array(
                    'ID' => $site_id,
                    'blog_details' => $blog_details,
                    'dfi' => get_the_post_thumbnail(),
                    'dfi_url' => get_the_post_thumbnail_url(),
                    'msi_url' => $multiSiteImageUrl,
                    'is_main' => false,
                );
            }
        }

        if(count($sites) > 1) {
            if($curr_site_array['ID'] != get_current_blog_id()) {
                switch_to_blog($curr_site_array['ID']);
            }
        }
    }

    if(is_bool($full) && !$full) {
        unset($sites[$main_site_id]);
        unset($sites[$curr_site_id]);
    }

    if($full == 'main') {
        $sites = $main_site_array;
    }

    if($full == 'self') {
        $sites = $curr_site_array;
    }

    return $sites;
}

function dws_simple_ms_main_blogname($blog_id = null) {
    $ms_blogInfo_name = '';

    $is_wpms = ( function_exists( 'is_multisite' ) && is_multisite() );

    $main_site_id = get_main_site_id();
    $curr_site_id = (!is_null($blog_id) && is_numeric($blog_id)) ? $blog_id : get_current_blog_id();

    $main_site_array = dws_simple_get_sites('main');

    /** @var $main_site_blogDetails WP_Site */
    $main_site_blogDetails = (is_array($main_site_array) && array_key_exists('blog_details', $main_site_array)) ? $main_site_array['blog_details'] : null;

    if($is_wpms && $main_site_id != $curr_site_id && is_object($main_site_blogDetails) && property_exists($main_site_blogDetails, 'blogname')) {
        $ms_blogInfo_name = $main_site_blogDetails->blogname;
    }

    return $ms_blogInfo_name;
}

function dws_simple_bloginfo_name($blog_id = null, $connector = '-') {

    $ms_blogInfo_name = dws_simple_ms_main_blogname($blog_id, $connector);

    $blogInfo_name = get_bloginfo( 'name', 'display' );

    if($ms_blogInfo_name != '' && $connector != '') {
        $blogInfo_name = $ms_blogInfo_name . ' ' . $connector . ' ' . $ms_blogInfo_name;
    }

    return $blogInfo_name;

}

/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 *
 * @param string $title Default title text for current view.
 * @return string The filtered title.
 */
function dws_simple_wp_title( $title ) {
    $sep = '&#8211;';

    if ( is_feed() ) {
        return $title;
    }

    global $page, $paged;

    $blogInfo_name = get_bloginfo( 'name', 'display' );

    $is_wpms = ( function_exists( 'is_multisite' ) && is_multisite() );

    $main_site_id = get_main_site_id();
    $curr_site_id = get_current_blog_id();

    $main_site_array = dws_simple_get_sites('main');

    /** @var $main_site_blogDetails WP_Site */
    $main_site_blogDetails = (is_array($main_site_array) && array_key_exists('blog_details', $main_site_array)) ? $main_site_array['blog_details'] : null;

    if($is_wpms && $main_site_id != $curr_site_id && is_object($main_site_blogDetails) && property_exists($main_site_blogDetails, 'blogname')) {
        $main_blog_name = $main_site_blogDetails->blogname;

        $curr_blogInfo_name = $blogInfo_name;

        $ms_blogInfo_name = ($main_blog_name != $curr_blogInfo_name) ? $main_blog_name . ' ' . $sep . ' ' . $curr_blogInfo_name : $curr_blogInfo_name;

        $blogInfo_name = $ms_blogInfo_name;
    }

    // Add the blog name
    $title .= $blogInfo_name;

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title .= " $sep $site_description";
    }

    // Add a page number if necessary:
    if ( ( $paged >= 2 || $page->ID >= 2 ) && ! is_404() ) {
        $title .= " $sep " . sprintf( __( 'Page %s', '_s' ), max( $paged, $page->ID ) );
    }
    return $title;
}
add_filter( 'pre_get_document_title', 'dws_simple_wp_title', 999, 1 );

/**
 * @wp-hook wp_loaded
 */
function dws_simple_wp_loaded() {

    add_action(
        'wpmu_new_blog',
        function( $blog_id, $user_id ) {

            switch_to_blog( $blog_id );

            $blog_name = dws_simple_bloginfo_name($blog_id);

            /** Update Option - Don't use Year Month Folders in Media Path  */
            update_option( 'uploads_use_yearmonth_folders', 0 );

            /** Update Option - Add Footer Left widget  */
            dws_simple_pre_set_widget( 'footer-left', 'text',
                array(
                    'title' => '',
                    'text' => '&copy; ' . date('Y') . ' ' . $blog_name,
                    'filter' => false,
                )
            );

            restore_current_blog();
        },
        10,
        2
    );
}
add_action( 'wp_loaded',  'dws_simple_wp_loaded' );

function dws_simple_pre_set_widget( $sidebar, $name, $args = array() ) {
    if ( ! $sidebars = get_option( 'sidebars_widgets' ) )
        $sidebars = array();

    // Create the sidebar if it doesn't exist.
    if ( ! isset( $sidebars[ $sidebar ] ) )
        $sidebars[ $sidebar ] = array();

    // Check for existing saved widgets.
    if ( $widget_opts = get_option( "widget_$name" ) ) {
        // Get next insert id.
        ksort( $widget_opts );
        end( $widget_opts );
        $insert_id = key( $widget_opts );
    } else {
        // None existing, start fresh.
        $widget_opts = array( '_multiwidget' => 1 );
        $insert_id = 0;
    }

    // Add our settings to the stack.
    $widget_opts[ ++$insert_id ] = $args;
    // Add our widget!
    $sidebars[ $sidebar ][] = "$name-$insert_id";

    update_option( 'sidebars_widgets', $sidebars );
    update_option( "widget_$name", $widget_opts );
}
