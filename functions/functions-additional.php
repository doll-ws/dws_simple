<?php
/**
 * functions-additional.php
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * Load Partial Template
 *
 * @param $slug
 * @param null $name
 */
function dws_simple_get_partial($slug, $name = null) {

    global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;

    if ( is_array( $wp_query->query_vars ) ) {
        extract( $wp_query->query_vars, EXTR_SKIP );
    }

    if ( isset( $s ) ) {
        $s = esc_attr( $s );
    }

    $name = (string) $name;

    $template = $slug . '.php';

    if ( '' !== $name ) {
        $template = $slug . '-' . $name . '.php';
    }

    $partialPath = locate_template( 'partials' . DIRECTORY_SEPARATOR . $template , false , false );
    $partialContent = '';

    if( file_exists($partialPath) ) {
        ob_start();
        include($partialPath);
        $partialContent = ob_get_contents();
        ob_end_clean();
    }

    echo $partialContent;
}