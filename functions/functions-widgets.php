<?php
/**
 * functions-widgets.php
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Widgets Init */
function dws_simple_widgets_init() {

    register_sidebar( array(
        'name'          => __( 'Sidebar - Default', 'dws_simple' ),
        'id'            => 'sidebar',
        'before_widget' => '<div class="sidebar-item sb-box">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => __('Sidebar - Home', 'dws_simple'),
        'id'            => 'home-sidebar',
        'before_widget' => '<div class="home-sb sidebar-item sb-box">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer - Top', 'dws_simple' ),
        'id'            => 'footer-top',
        'before_widget' => '<div class="ft-item sb-box">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer - Left', 'dws_simple' ),
        'id'            => 'footer-left',
        'before_widget' => '<div class="fl-item">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer - Right', 'dws_simple' ),
        'id'            => 'footer-right',
        'before_widget' => '<div class="fr-item">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );

}
add_action( 'widgets_init', 'dws_simple_widgets_init' );