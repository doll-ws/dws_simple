<?php
/**
 * functions-debug.php
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */


function post_get_debug() {

    $debugOutput = array();

    $customized = dws_simple_get_customized();
    if(count($customized)) {
        $debugOutput['customized'] = $customized;
        $debugOutput['customized_get'] = http_build_query($customized);

        echo '<pre>' . print_r($debugOutput, true) . '</pre>';
    }

}
add_action( 'wp_head', 'post_get_debug' );

function send_debug_mail($message = '') {
    $empfaenger = 'info@doll-ws.com';

// Betreff
    $betreff = 'Doll WebSolutions - Debug Mail';

// Nachricht
    $nachricht = '
<html>
<head>
  <title>' . $betreff . '</title>
</head>
<body>
  ' . $message . '
</body>
</html>
';

// für HTML-E-Mails muss der 'Content-type'-Header gesetzt werden
    $header[] = 'MIME-Version: 1.0';
    $header[] = 'Content-type: text/html; charset=iso-8859-1';

// verschicke die E-Mail
    mail($empfaenger, $betreff, $nachricht, implode("\r\n", $header));
}
