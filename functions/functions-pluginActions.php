<?php
/**
 * functions-pluginActions.php
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Check fpr IsPluginActive Function */
if (!function_exists('is_plugin_active')) {
    include_once(ABSPATH . 'wp-admin/includes/plugin.php');
}

add_action('admin_notices', 'dws_simple_theme_dependencies');

function dws_simple_theme_dependencies()
{
    $dependencyPlugins = array();

    $dependencyPlugins[] = array(
        'code' => 'slick-slider',
        'path' => 'slick-slider/slick-slider.php',
        'name' => 'Sick Slider',
        'url'  => 'https://wordpress.org/plugins/slick-slider/',
    );

    $dependencyPlugins[] = array(
        'code' => 'advanced-custom-fields',
        'path' => 'advanced-custom-fields/acf.php',
        'name' => 'Advanced Custom Fields (ACF)',
        'url'  => 'https://wordpress.org/plugins/advanced-custom-fields/',
    );

    foreach ($dependencyPlugins as $plugin) {
        if (!is_plugin_active($plugin['path'])) {
            echo '<div class="error" data-plugincode="' . $plugin['code']  . '"><p>' . sprintf(__('Warning: The theme needs Plugin <a href="%s" target="_blank">%s</a> to function.', 'dws_simple'), $plugin['url'], $plugin['name']) . '</p></div>';
        }
    }
}

/**
 * Get Post Primary Category
 *
 * @param $post_id
 * @return array|null|object|WP_Error
 */
function dws_simple_getPostPrimaryCategory($post_id)
{

    $primary_cat_Ids = get_post_meta($post_id, '_yoast_wpseo_primary_category', true);
    if ($primary_cat_Ids != null) {
        $cat_ids = $primary_cat_Ids;
    } else {
        $post_categories = wp_get_post_categories($post_id);
        $cat_ids = array();

        foreach ($post_categories as $c) {
            $cat = get_category($c);
            $cat_ids[] = $cat->cat_ID;
        }
    }

    $firstCatId = (count($cat_ids)) ? $cat_ids[0] : 0;

    $category = get_category($firstCatId);

    return $category;
}

/** Include Slick Slider Scripts */
if (is_plugin_active('slick-slider/slick-slider.php')) {
    add_action('wp_enqueue_scripts', function () {
        wp_enqueue_script('slick-slider-core');
        wp_enqueue_style('slick-slider-core-theme');
    }, 11);
}

/** Register AdvanceCustomFields Field Groups */
if (function_exists("register_field_group")) {
    /**
     * Shown Title
     *
     * Title Fields for shown Title and SubTitle
     */
    register_field_group(array (
        'id' => 'acf_shown_title',
        'title' => __('Shown Title', 'dws_simple'),
        'fields' => array (
            array (
                'key' => 'field_5ae19670b1368',
                'label' => __('Shown Title', 'dws_simple'),
                'name' => 'shown_title',
                'type' => 'text',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_5ae196a0b1369',
                'label' => __('Sub Title', 'dws_simple'),
                'name' => 'subtitle',
                'type' => 'text',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'acf_after_title',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));

    /**
     * Home Slider
     *
     * Maximum of 5 Items with 3 Layout Types and Linking to Posts and Pages
     */
    register_field_group(array (
        'id' => 'acf_home-slider',
        'title' => 'Home Slider',
        'fields' => array (

            /** Item 01 */
            array (
                'key' => 'field_5ad222b0a5a74',
                'label' => __('Item', 'dws_simple') . ' 01',
                'name' => '',
                'type' => 'tab',
            ),
            array (
                'key' => 'field_5ad22405a5a79',
                'label' => 'Layout',
                'name' => 'slider_item_01_layout',
                'type' => 'select',
                'required' => 1,
                'choices' => array (
                    'only_image' => __('Only Image', 'dws_simple'),
                    'left' => __('Text Left', 'dws_simple'),
                    'right' => __('Text Right', 'dws_simple'),
                ),
                'default_value' => 'only_image',
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5ad222d4a5a75',
                'label' => __('Image', 'dws_simple'),
                'name' => 'slider_item_01_image',
                'type' => 'image',
                'save_format' => 'object',
                'preview_size' => 'thumbnail',
                'library' => 'uploadedTo',
            ),
            array (
                'key' => 'field_5ad2232ba5a77',
                'label' => __('Title', 'dws_simple'),
                'name' => 'slider_item_01_title',
                'type' => 'text',
                'conditional_logic' => array (
                    'status' => 1,
                    'rules' => array (
                        array (
                            'field' => 'field_5ad22405a5a79',
                            'operator' => '!=',
                            'value' => 'only_image',
                        ),
                    ),
                    'allorany' => 'all',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_5ad22378a5a78',
                'label' => __('Text', 'dws_simple'),
                'name' => 'slider_item_01_text',
                'type' => 'wysiwyg',
                'conditional_logic' => array (
                    'status' => 1,
                    'rules' => array (
                        array (
                            'field' => 'field_5ad22405a5a79',
                            'operator' => '!=',
                            'value' => 'only_image',
                        ),
                    ),
                    'allorany' => 'all',
                ),
                'default_value' => '',
                'toolbar' => 'basic',
                'media_upload' => 'no',
            ),
            array (
                'key' => 'field_5ad22a942f22e',
                'label' => __('Link', 'dws_simple'),
                'name' => 'slider_item_01_link',
                'type' => 'link',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'url',
            ),

            /** Item 02 */
            array (
                'key' => 'field_5ad225ceb6aae',
                'label' => __('Item', 'dws_simple') . ' 02',
                'name' => '',
                'type' => 'tab',
            ),
            array (
                'key' => 'field_5ad22729b6ab0',
                'label' => 'Layout',
                'name' => 'slider_item_02_layout',
                'type' => 'select',
                'required' => 1,
                'choices' => array (
                    'only_image' => __('Only Image', 'dws_simple'),
                    'left' => __('Text Left', 'dws_simple'),
                    'right' => __('Text Right', 'dws_simple'),
                ),
                'default_value' => 'only_image',
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5ad225fbb6aaf',
                'label' => __('Image', 'dws_simple'),
                'name' => 'slider_item_02_image',
                'type' => 'image',
                'save_format' => 'object',
                'preview_size' => 'thumbnail',
                'library' => 'uploadedTo',
            ),
            array (
                'key' => 'field_5ad22791b6ab1',
                'label' => __('Title', 'dws_simple'),
                'name' => 'slider_item_02_title',
                'type' => 'text',
                'conditional_logic' => array (
                    'status' => 1,
                    'rules' => array (
                        array (
                            'field' => 'field_5ad22729b6ab0',
                            'operator' => '!=',
                            'value' => 'only_image',
                        ),
                    ),
                    'allorany' => 'all',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_5ad227c1b6ab2',
                'label' => __('Text', 'dws_simple'),
                'name' => 'slider_item_02_text',
                'type' => 'wysiwyg',
                'conditional_logic' => array (
                    'status' => 1,
                    'rules' => array (
                        array (
                            'field' => 'field_5ad22729b6ab0',
                            'operator' => '!=',
                            'value' => 'only_image',
                        ),
                    ),
                    'allorany' => 'all',
                ),
                'default_value' => '',
                'toolbar' => 'basic',
                'media_upload' => 'no',
            ),
            array (
                'key' => 'field_5ad22ad52f22f',
                'label' => __('Link', 'dws_simple'),
                'name' => 'slider_item_02_link',
                'type' => 'link',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'url',
            ),

            /** Item 03 */
            array (
                'key' => 'field_5ad22c91a9707',
                'label' => __('Item', 'dws_simple') . ' 03',
                'name' => '',
                'type' => 'tab',
            ),
            array (
                'key' => 'field_5ad22caca9708',
                'label' => 'Layout',
                'name' => 'slider_item_03_layout',
                'type' => 'select',
                'required' => 1,
                'choices' => array (
                    'only_image' => __('Only Image', 'dws_simple'),
                    'left' => __('Text Left', 'dws_simple'),
                    'right' => __('Text Right', 'dws_simple'),
                ),
                'default_value' => 'only_image',
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5ad22ccfa9709',
                'label' => __('Image', 'dws_simple'),
                'name' => 'slider_item_03_image',
                'type' => 'image',
                'save_format' => 'object',
                'preview_size' => 'thumbnail',
                'library' => 'uploadedTo',
            ),
            array (
                'key' => 'field_5ad22d01a970a',
                'label' => __('Title', 'dws_simple'),
                'name' => 'slider_item_03_title',
                'type' => 'text',
                'conditional_logic' => array (
                    'status' => 1,
                    'rules' => array (
                        array (
                            'field' => 'field_5ad22caca9708',
                            'operator' => '!=',
                            'value' => 'only_image',
                        ),
                    ),
                    'allorany' => 'all',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_5ad22d28a970b',
                'label' => __('Text', 'dws_simple'),
                'name' => 'slider_item_03_text',
                'type' => 'wysiwyg',
                'conditional_logic' => array (
                    'status' => 1,
                    'rules' => array (
                        array (
                            'field' => 'field_5ad22caca9708',
                            'operator' => '!=',
                            'value' => 'only_image',
                        ),
                    ),
                    'allorany' => 'all',
                ),
                'default_value' => '',
                'toolbar' => 'basic',
                'media_upload' => 'no',
            ),
            array (
                'key' => 'field_5ad22d5aa970c',
                'label' => __('Link', 'dws_simple'),
                'name' => 'slider_item_03_link',
                'type' => 'link',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'url',
            ),

            /** Item 04 */
            array (
                'key' => 'field_5ad22e40a970d',
                'label' => __('Item', 'dws_simple') . ' 04',
                'name' => '',
                'type' => 'tab',
            ),
            array (
                'key' => 'field_5ad22e72a970e',
                'label' => 'Layout',
                'name' => 'slider_item_04_layout',
                'type' => 'select',
                'required' => 1,
                'choices' => array (
                    'only_image' => __('Only Image', 'dws_simple'),
                    'left' => __('Text Left', 'dws_simple'),
                    'right' => __('Text Right', 'dws_simple'),
                ),
                'default_value' => 'only_image',
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5ad22ec3a970f',
                'label' => __('Image', 'dws_simple'),
                'name' => 'slider_item_04_image',
                'type' => 'image',
                'save_format' => 'object',
                'preview_size' => 'thumbnail',
                'library' => 'uploadedTo',
            ),
            array (
                'key' => 'field_5ad22f08a9710',
                'label' => __('Title', 'dws_simple'),
                'name' => 'slider_item_04_title',
                'type' => 'text',
                'conditional_logic' => array (
                    'status' => 1,
                    'rules' => array (
                        array (
                            'field' => 'field_5ad22e72a970e',
                            'operator' => '!=',
                            'value' => 'only_image',
                        ),
                    ),
                    'allorany' => 'all',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_5ad22f29a9711',
                'label' => __('Text', 'dws_simple'),
                'name' => 'slider_item_04_text',
                'type' => 'wysiwyg',
                'conditional_logic' => array (
                    'status' => 1,
                    'rules' => array (
                        array (
                            'field' => 'field_5ad22e72a970e',
                            'operator' => '!=',
                            'value' => 'only_image',
                        ),
                    ),
                    'allorany' => 'all',
                ),
                'default_value' => '',
                'toolbar' => 'basic',
                'media_upload' => 'no',
            ),
            array (
                'key' => 'field_5ad22f4ba9712',
                'label' => __('Link', 'dws_simple'),
                'name' => 'slider_item_04_link',
                'type' => 'link',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'url',
            ),

            /** Item 05 */
            array (
                'key' => 'field_5ad22f6ea9713',
                'label' => __('Item', 'dws_simple') . ' 05',
                'name' => '',
                'type' => 'tab',
            ),
            array (
                'key' => 'field_5ad22f86a9714',
                'label' => 'Layout',
                'name' => 'slider_item_05_layout',
                'type' => 'select',
                'required' => 1,
                'choices' => array (
                    'only_image' => __('Only Image', 'dws_simple'),
                    'left' => __('Text Left', 'dws_simple'),
                    'right' => __('Text Right', 'dws_simple'),
                ),
                'default_value' => 'only_image',
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_5ad22faca9715',
                'label' => __('Image', 'dws_simple'),
                'name' => 'slider_item_05_image',
                'type' => 'image',
                'save_format' => 'object',
                'preview_size' => 'thumbnail',
                'library' => 'uploadedTo',
            ),
            array (
                'key' => 'field_5ad22fcea9716',
                'label' => __('Title', 'dws_simple'),
                'name' => 'slider_item_05_title',
                'type' => 'text',
                'conditional_logic' => array (
                    'status' => 1,
                    'rules' => array (
                        array (
                            'field' => 'field_5ad22f86a9714',
                            'operator' => '!=',
                            'value' => 'only_image',
                        ),
                    ),
                    'allorany' => 'all',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_5ad22ff5a9717',
                'label' => __('Text', 'dws_simple'),
                'name' => 'slider_item_05_text',
                'type' => 'wysiwyg',
                'conditional_logic' => array (
                    'status' => 1,
                    'rules' => array (
                        array (
                            'field' => 'field_5ad22f86a9714',
                            'operator' => '!=',
                            'value' => 'only_image',
                        ),
                    ),
                    'allorany' => 'all',
                ),
                'default_value' => '',
                'toolbar' => 'basic',
                'media_upload' => 'no',
            ),
            array (
                'key' => 'field_5ad23015a9718',
                'label' => __('Link', 'dws_simple'),
                'name' => 'slider_item_05_link',
                'type' => 'link',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'url',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'page_type',
                    'operator' => '==',
                    'value' => 'front_page',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'acf_after_title',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 1,
    ));

    /**
     * Before Content
     *
     * Add Contents before Content
     */
    register_field_group(array (
        'id' => 'acf_before-content',
        'title' => __('Before Content', 'dws_simple'),
        'fields' => array (
            array (
                'key' => 'field_5b504d4ee0c46',
                'label' => __('Before Content', 'dws_simple'),
                'name' => 'before_content',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'basic',
                'media_upload' => 'yes',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'acf_after_title',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 2,
    ));

    /**
     * After Content
     *
     * Add Contents after Content
     */
    register_field_group(array (
        'id' => 'acf_after-content',
        'title' => __('After Content', 'dws_simple'),
        'fields' => array (
            array (
                'key' => 'field_5b504ef4ef234',
                'label' => __('After Content', 'dws_simple'),
                'name' => 'after_content',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'basic',
                'media_upload' => 'yes',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
                array (
                    'param' => 'post_type',
                    'operator' => '!=',
                    'value' => 'front_page',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));


    $is_wpms = ( function_exists('is_multisite') && is_multisite() );
    $sites   = ( function_exists('dws_simple_get_sites') ) ? dws_simple_get_sites(false) : array();

    if ($is_wpms && count($sites)) {

        /**
         * MultiSite Image
         *
         * Image to show on MultiSite Homepage
         */
        register_field_group(array (
            'id' => 'acf_multi-site-image',
            'title' => __('MultiSite Image'),
            'fields' => array (
                array (
                    'key' => 'field_5b87bb3c76c75',
                    'label' => __('MultiSite Image', 'dws_simple'),
                    'name' => 'multi-site-image',
                    'type' => 'image',
                    'save_format' => 'url',
                    'preview_size' => 'thumbnail',
                    'library' => 'uploadedTo',
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'page_type',
                        'operator' => '==',
                        'value' => 'front_page',
                        'order_no' => 0,
                        'group_no' => 0,
                    ),
                ),
            ),
            'options' => array (
                'position' => 'side',
                'layout' => 'default',
                'hide_on_screen' => array (
                ),
            ),
            'menu_order' => 0,
        ));
    }


    /**
     * Home Teaser Box
     *
     * Teaser Boxes from content Pages
     */
    register_field_group(array (
        'id' => 'acf_home-teaser-box',
        'title' => 'Home-Teaser Box',
        'fields' => array (
            array (
                'key' => 'field_58ed516da3d35',
                'label' => __('Visible', 'dws_simple'),
                'name' => 'visible',
                'type' => 'true_false',
                'message' => __('Visible', 'dws_simple'),
                'default_value' => 0,
            ),
            array (
                'key' => 'field_58ed50a5a3d31',
                'label' => __('Image', 'dws_simple'),
                'name' => 'image',
                'type' => 'image',
                'conditional_logic' => array (
                    'status' => 1,
                    'rules' => array (
                        array (
                            'field' => 'field_58ed516da3d35',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                    'allorany' => 'all',
                ),
                'save_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'uploadedTo',
            ),
            array (
                'key' => 'field_58ed511ea3d33',
                'label' => __('Text', 'dws_simple'),
                'name' => 'text',
                'type' => 'text',
                'conditional_logic' => array (
                    'status' => 1,
                    'rules' => array (
                        array (
                            'field' => 'field_58ed516da3d35',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                    'allorany' => 'all',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_58ee328ec84c5',
                'label' => __('Sorting', 'dws_simple'),
                'name' => 'sorting',
                'type' => 'number',
                'conditional_logic' => array (
                    'status' => 1,
                    'rules' => array (
                        array (
                            'field' => 'field_58ed516da3d35',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                    'allorany' => 'all',
                ),
                'default_value' => 0,
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'min' => '',
                'max' => '',
                'step' => '',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
                array (
                    'param' => 'page_type',
                    'operator' => '!=',
                    'value' => 'front_page',
                    'order_no' => 1,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'side',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));

    /**
     * Page Excerpt
     *
     * Add Page Excerpt if necessary
     */
    register_field_group(array(
        'key' => 'acf_page-excerpt',
        'title' => 'Page Excerpt',
        'fields' => array(
            array(
                'key' => 'field_5fc1416e06388',
                'label' => 'Page Excerpt',
                'name' => 'page_excerpt',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array (
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => '',
                'new_lines' => '',
            ),
        ),
        'location' => array(
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
                array (
                    'param' => 'page_type',
                    'operator' => '!=',
                    'value' => 'front_page',
                    'order_no' => 1,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'side',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}

/** Remove DFI from Posts */
if (is_plugin_active('default-featured-image/set-default-featured-image.php')) {
    function dws_simple_dfi_remove_from_posts($dfi_id, $post_id)
    {
        $post = get_post($post_id);
        if (is_object($post) && 'post' === $post->post_type) {
            return 0; //invalid ID
        }
        return $dfi_id; // the original featured image id
    }
    add_filter('dfi_thumbnail_id', 'dws_simple_dfi_remove_from_posts', 10, 2);
}
