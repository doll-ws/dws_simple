<?php
/**
 * functions-setup.php
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** WP Setup */
function dws_simple_wp_setup()
{
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    load_theme_textdomain('dws_simple', get_template_directory() . '/languages');
}
add_action('after_setup_theme', 'dws_simple_wp_setup');

/**
 * Customize the title.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @param string $sepLocation Optional where to put the separator rom BlogName.
 *
 * @return string The title to use.
 */
function dws_wp_title(string $title, string $sep = '&ndash;', string $sepLocation = 'left'): string
{
    if (is_feed()) {
        return $title;
    }

    if (is_home() || is_front_page()) {
        return get_bloginfo('name');
    }

    $page         = get_post();
    $theTitle     = the_title('', '', false);
    $shownTitle   = get_field('shown_title', $page->ID);
    $curPageTitle = $shownTitle !== '' ? $shownTitle : $theTitle;
    $titleParts   = [$curPageTitle, get_bloginfo('name')];
    $sepLocation  = ($sepLocation !== 'left' && $sepLocation !== 'right') ? 'left' : $sepLocation;

    $titleParts = ($sepLocation === 'right') ? array_reverse($titleParts) : $titleParts;

    return implode(' ' . $sep . ' ', $titleParts);
}

add_filter('wp_title', 'dws_wp_title', 10, 3);
