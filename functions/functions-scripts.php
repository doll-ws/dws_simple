<?php
/**
 * functions-scripts.php
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Add Styles and Scrips */
function dws_simple_enqueue_styles()
{
    wp_register_style('bootstrap', get_template_directory_uri() . '/inc/bootstrap/css/bootstrap.min.css');
    $dependencies = array('bootstrap');
    wp_enqueue_style('dws_simple-static-style', get_stylesheet_uri(), $dependencies);

    $customized_query = '';
    if (function_exists('dws_simple_get_customized')) {
        $customized = dws_simple_get_customized();
        if (count($customized)) {
            $customized_query = '?' . http_build_query($customized);
        }
    }

    wp_enqueue_style(
        'dws_simple_styles',
        get_template_directory_uri() . '/assets/css/styles.css.php' . $customized_query
    );
}
function dws_simple_enqueue_scripts()
{
    $dependencies = array('jquery');
    wp_enqueue_script(
        'bootstrap',
        get_template_directory_uri() . '/inc/bootstrap/js/bootstrap.min.js',
        $dependencies,
        '',
        true
    );
    wp_enqueue_script(
        'dws_simple_scripts',
        get_template_directory_uri() . '/assets/js/scripts.js',
        $dependencies,
        '',
        true
    );
}
add_action('wp_enqueue_scripts', 'dws_simple_enqueue_styles');
add_action('wp_enqueue_scripts', 'dws_simple_enqueue_scripts');

add_action('wp_enqueue_scripts', function () {
    wp_add_inline_script(
        'jquery-migrate',
        'jQuery.migrateMute = true;',
        'before'
    );
});
