<?php
/**
 * functions-menu.php
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Include Bootstrap Nav-Walker */
$themePath = get_template_directory();
require_once( $themePath . '/inc/wp-bootstrap-navwalker.php');

/** Register Main Menu */
function dws_simple_register_menu() {
    register_nav_menu('header-menu', __( 'Header Menu', 'dws_simple' ));
}
add_action( 'init', 'dws_simple_register_menu' );
