<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php
/**
 * Theme Part: Header
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

$ms_blogInfo_name = ( function_exists( 'dws_simple_ms_main_blogname' ) ) ? dws_simple_ms_main_blogname() : '';

//$blogInfo_name = ( function_exists( 'dws_simple_bloginfo_name' ) && dws_simple_bloginfo_name() !== '' ) ? dws_simple_bloginfo_name() : get_bloginfo( 'name' );
$blogInfo_name = get_bloginfo( 'name' );
$blogInfo_desc = ( $ms_blogInfo_name != '' ) ? ( get_bloginfo('description') != '' ) ? $ms_blogInfo_name . ' - ' . get_bloginfo('description') : $ms_blogInfo_name : get_bloginfo('description');

$headerLayout = get_theme_mod( 'header_layout' , 'container' );

$headerLogoCols           = ($headerLayout != 'container') ? 'col-sm-5' : 'col-sm-5';
$headerLogoInnerLeftCols  = ($headerLayout != 'container') ? 'col-sm-2' : 'col-sm-3';
$headerLogoInnerRightCols = ($headerLayout != 'container') ? 'col-sm-10' : 'col-sm-9';
$navBarCols               = ($headerLayout != 'container') ? 'col-sm-7' : 'col-sm-7';

?>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php wp_title('-'); ?></title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<a name="top" id="top"></a>
<div class="wrapper">
    <header class="header">
        <div class="<?php echo $headerLayout ?>">
            <div class="row">
                <div class="header-logo <?php echo $headerLogoCols ?>">
                    <div class="row">
                        <div class="<?php echo $headerLogoInnerLeftCols ?>">
                            <a href="<?php echo home_url(); ?>" title="<?php echo $blogInfo_name; ?>">
                                <?php $logoSrc = (get_theme_mod( 'page_logo' )) ? get_theme_mod( 'page_logo' ) : get_template_directory_uri() . '/assets/img/logo.png'; ?>
                                <img class="header-logo-image img-responsive" src="<?php echo esc_url( $logoSrc ); ?>" alt="" />
                            </a>
                        </div>
                        <div class="<?php echo $headerLogoInnerRightCols ?>">
                            <a href="<?php echo home_url(); ?>" title="<?php echo $blogInfo_name; ?>">
                                <span>
                                    <?php echo $blogInfo_name; ?>
                                    <?php if($blogInfo_desc != '') : ?><small><?php echo $blogInfo_desc; ?></small><?php endif; ?>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>

                <nav class="navbar navbar-default <?php echo $navBarCols ?>">
                    <div class="navbar-header visible-xs-block">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only"><?php echo __( 'Navigation', 'dws_simple' ); ?></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand hidden-xs" href="<?php echo home_url(); ?>">
                            <?php echo $blogInfo_name; ?>
                        </a>
                    </div>


                    <?php
                    wp_nav_menu( array(
                            'menu'              => 'header-menu',
                            'theme_location'    => 'header-menu',
                            'depth'             => 2,
                            'container'         => 'div',
                            'container_class'   => 'collapse navbar-collapse',
                            'container_id'      => 'navbar',
                            'menu_class'        => 'nav navbar-nav',
                            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                            'walker'            => new WP_Bootstrap_Navwalker())
                    );
                    ?>
                </nav>
            </div>
        </div>
    </header>
