<?php
/*
Template Name: DS - Homepage
*/

/**
 * Page Template: Homepage
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */
?>

<?php
    $page              = get_post();

    $theTitle          = the_title('','',false);
    $shownTitle        = get_field( "shown_title", $page->ID );
    $curPageTitle      = ($shownTitle != '') ? $shownTitle : $theTitle;
    $subTitle          = get_field( "subtitle", $page->ID );
    $subTitleOutput    = ($subTitle != '') ? ' <small>' . $subTitle . '</small>' : '';

    $pageHeaderWsClass = ($subTitle != '') ? ' ws' : '';

    $isHomeSidebar      = (is_active_sidebar('home-sidebar'));
    $getShowHomeSidebar = (get_theme_mod( 'home_sidebar' , 0 ) > 0);
    $showHomeSidebar    = ($isHomeSidebar && $getShowHomeSidebar);

    $getShowHomeTeaser    = (get_theme_mod( 'home_teaser' , 1 ) > 0);
    $getHomeTeaserVersion = get_theme_mod( 'home_teaser_version' , 1 );

    $mainColWidth       = ($showHomeSidebar) ? '8' : '12';

    $getShowHomeNews    = (get_theme_mod( 'home_news' , 1 ) > 0);
?>

<?php get_header(); ?>

<?php get_template_part( 'partials/home', 'slider' ); ?>

<?php get_template_part( 'partials/home', 'sites' ); ?>

<?php get_template_part( 'partials/home', 'infoblock' ); ?>

<?php
    if ($getShowHomeTeaser) {
        get_template_part('partials/home', 'teaser_v' . $getHomeTeaserVersion);
    }
?>

<?php get_template_part( 'partials/content', 'before' ); ?>

    <section class="content-wrapper cleafix">
        <div id="content" class="<?php echo get_theme_mod( 'page_layout' , 'container' ); ?>">
            <div class="row">

                <div class="content-inner col-sm-<?php echo $mainColWidth; ?>">
                    <h1 class="page-header<?php echo $pageHeaderWsClass; ?>"><?php echo $curPageTitle.$subTitleOutput; ?></h1>
                    <?php while (have_posts()) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; ?>

                    <?php if($getShowHomeNews) : ?>
                        <div class="row">
                            <?php get_template_part( 'partials/posts', 'home' ); ?>
                        </div>
                    <?php endif; ?>
                </div>

                <?php if ($showHomeSidebar) : ?>
                <div class="sidebar col-sm-4">
                    <?php dynamic_sidebar( 'home-sidebar' ); ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </section>

<?php dws_simple_get_partial( 'content', 'after' ); ?>

<?php get_footer(); ?>