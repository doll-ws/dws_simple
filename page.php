<?php
/**
 * Theme Part: Page
 *
 * @package Doll WebSolutions
 * @subpackage Simple Theme
 * @version 3.1
 *
 * @author Doll WebSolutions <info@doll-ws.com>
 * @copyright 2018 by Doll WebSolutions
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */
?>
<?php
    $page = get_post();
    $theTitle = the_title('','',false);
    $shownTitle = get_field( "shown_title", $page->ID );
    $curPageTitle = ($shownTitle != '') ? $shownTitle : $theTitle;
    $subTitle          = get_field( "subtitle", $page->ID );
    $subTitleOutput    = ($subTitle != '') ? ' <small>' . $subTitle . '</small>' : '';

    $isActiveSideBar = is_active_sidebar('sidebar');

    $contentInnerColClass = ($isActiveSideBar) ? 'col-sm-8' : 'col-sm-12';
?>

<?php get_header(); ?>

<?php get_template_part( 'partials/content', 'before' ); ?>

    <section class="content-wrapper cleafix">
        <div id="content" class="<?php echo get_theme_mod( 'page_layout' , 'container' ); ?>">
            <div class="row">

                <div class="content-inner <?php echo $contentInnerColClass; ?>">
                    <h1 class="page-header"><?php echo $curPageTitle.$subTitleOutput; ?></h1>
                    <?php while (have_posts()) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; ?>
                </div>
                <?php if($isActiveSideBar) : ?>
                <div class="sidebar col-sm-4">
                    <?php get_sidebar(); ?>
                </div>
                <?php endif; ?>

            </div>
        </div>
    </section>

<?php get_template_part( 'partials/content', 'after' ); ?>

<?php get_footer(); ?>